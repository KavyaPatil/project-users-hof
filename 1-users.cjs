const users = {
  John: {
    age: 24,
    desgination: "Senior Golang Developer",
    interests: ["Chess, Reading Comics, Playing Video Games"],
    qualification: "Masters",
    nationality: "Greenland",
  },
  Ron: {
    age: 19,
    desgination: "Intern - Golang",
    interests: ["Video Games"],
    qualification: "Bachelor",
    nationality: "UK",
  },
  Wanda: {
    age: 24,
    desgination: "Intern - Javascript",
    interests: ["Piano"],
    qualification: "Bachaelor",
    nationality: "Germany",
  },
  Rob: {
    age: 34,
    desgination: "Senior Javascript Developer",
    interest: ["Walking his dog, Cooking"],
    qualification: "Masters",
    nationality: "USA",
  },
  Pike: {
    age: 23,
    desgination: "Python Developer",
    interests: ["Listing Songs, Watching Movies"],
    qualification: "Bachaelor's Degree",
    nationality: "Germany",
  },
};

/*


Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/

//Q1 Find all users who are interested in playing video games.

function usersInterests(users) {
  const usersInterestedinVideoaGames = {};

  let keys = Object.keys(users);

  keys.map((user) => {
    let interestString = "";

    if (users[user].interests) {
      interestString = users[user].interests.join();
    } else if (users[user].interest) {
      interestString = users[user].interest.join();
    }

    if (interestString.includes("Video Games")) {
      usersInterestedinVideoaGames[user] = users[user];
    }
  });

  return usersInterestedinVideoaGames;
}
console.log(usersInterests(users));

//  Q2 Find all users staying in Germany.

function usersStayingInGermanyFunction(users) {
  const usersStayingInTheGermany = {};

  const userKeys = Object.keys(users);

  userKeys.map((user) => {
    if (users[user].nationality === "Germany") {
      usersStayingInTheGermany[user] = users[user];
    }
  });

  return usersStayingInTheGermany;
}

console.log(usersStayingInGermanyFunction(users));

/* 

Q3 Sort users based on their seniority level .
 for Designation - Senior Developer > Developer > Intern
for Age - 20 > 10

*/

function sortAccordingToSeniority(users) {
  const sortedUsersObject = {};

  const usersentris = Object.entries(users);

  const array = usersentris.sort((user1, user2) => {
    console.log(user1[1].desgination.includes("Senior"))
    if ((user1[1].desgination.includes("Senior") && user2[1].desgination.includes("Developer")) ||(user1[1].desgination.includes("Senior Developer") &&user2[1].desgination.includes("Intern")) ||(user1[1].desgination.includes("Developer") && user2[1].desgination.includes("Intern"))) {
      return -1 ;
    }else if((user1[1].desgination.includes("Senior Developer") && user2[1].desgination.includes("Senior Developer")) || (user1[1].desgination.includes("Developer") && user2[1].desgination.includes("Developer")) ||(user1[1].desgination.includes("Intern") &&user2[1].desgination.includes("Intern"))){
        if(user1[1].age>user2[1].age){
        return -1;
     }else{
        return user2 ;
     }
    }else{
        return 1;
    }
   
  });

  console.log(Object.fromEntries(array));
}

sortAccordingToSeniority(users);
